FROM ruby:2.4-rc
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev
RUN mkdir /peach-heart
WORKDIR /peach-heart
ADD Gemfile /peach-heart/Gemfile
ADD Gemfile.lock /peach-heart/Gemfile.lock
RUN bundle install
ADD . /peach-heart
